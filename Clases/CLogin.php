<?php
require_once 'Clases/CBase.php';
require_once 'Clases/CSql.php';
class CLogin extends CBase {
   public $paData, $paDatos, $paUniAca;

   public function __construct() {
      parent::__construct();
      $this->paData = $this->paDatos = $this->paUniAca = null;
   }

   public function omLogin() {
      $llOk = $this->mxValParamLogin();
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxLogin($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   protected function mxValParamLogin() {
      if (!isset($this->paData['CIDUSUA']) || empty($this->paData['CIDUSUA'])) {
         $this->pcError = "LLENE LOS CAMPOS";
         return false;
      }
      if (!isset($this->paData['CCONTRA']) || empty($this->paData['CCONTRA'])) {
         $this->pcError = "LLENE LOS CAMPOS";
         return false;
      }
      return true;
   }
   protected function mxLogin($p_oSql) {
      $lcJson = json_encode($this->paData);
      $lcSql = "SELECT P_LOGIN('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "ERROR AL INICIAR SESION";
         return false;
      }
      $laData = json_decode($laFila[0], true);
      if (isset($laData['ERROR'])) {
         $this->pcError = $laData['ERROR'];
         return false;
      }
      $this->paDatos = $laData;
      return true;
   }
}
?>