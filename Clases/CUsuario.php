<?php
require_once 'Clases/CBase.php';
require_once 'Clases/CSql.php';
class CUsuario extends CBase {
   public $paData, $paDatos, $paUniAca;

   public function __construct() {
      parent::__construct();
      $this->paData = $this->paDatos = $this->paUniAca = null;
   }
   public function omRegistrarUsuario() {
      $llOk = $this->mxValParamRegistrarUsuario();      
      if (!$llOk) {
         return false;
      }
      $loSql = new CSql();
      $llOk = $loSql->omConnect();
      if (!$llOk) {
         $this->pcError = $loSql->pcError;
         return false;
      }
      $llOk = $this->mxRegistrarUsuario($loSql);
      $loSql->omDisconnect();
      return $llOk;
   }
   protected function mxValParamRegistrarUsuario() {
      if (!isset($this->paData['CNOMBRE'])) {
         $this->pcError = "NOMBRE NO DEFINIDO";
         return false;
      }
      if (!isset($this->paData['CCELULA']) || !preg_match('/^[0-9]{9}$/', $this->paData['CCELULA'])) {
         $this->pcError = "CELULAR NO DEFINIDO O NO VALIDO";
         return false;
      }
      if (!isset($this->paData['CAÑNACI']) || !preg_match('/^[0-9]{4}$/', $this->paData['CAÑNACI'])) {
         $this->pcError = "AÑO DE NACIMIENTO NO DEFINIDO O NO VALIDO";
         return false;
      }
      if (!isset($this->paData['CCORREO']) || !preg_match('/^.*@.*\..*$/', $this->paData['CCORREO'])) {
         $this->pcError = "CORREO ELECTRONICO NO DEFINIDO O NO VALIDO";
         return false;
      }
      if (!isset($this->paData['CCONTRA'])) {
         $this->pcError = "CONTRASEÑA NO DEFINIDA";
         return false;
      }
      if (isset($this->paFile['BPHOTO']['name']) && !empty($this->paFile['BPHOTO']['name'])) {
         preg_match('/(.*)@/', $this->paData['CCORREO'], $matches);
         if (count($matches) < 2) {
            $this->pcError = "ERROR CONSEGUIR NICKNAME";
            return false;
         }
         preg_match('/.*\.(.*)/', $this->paFile['BPHOTO']['name'], $matches);
         if (count($matches) < 2) {
            $this->pcError = "ERROR CONSEGUIR EXTENSION DEL ARCHIVO";
            return false;
         }
      }
      return true;
   }
   protected function mxRegistrarUsuario($p_oSql) {
      if (isset($this->paFile['BPHOTO']['name']) && !empty($this->paFile['BPHOTO']['name'])) {
         preg_match('/(.*)@/', $this->paData['CCORREO'], $matches);
         $lcUsername = $matches[1];
         preg_match('/.*\.(.*)/', $this->paFile['BPHOTO']['name'], $matches);
         $lcExtension = $matches[1];
         $lcFoto = $lcUsername.".".$lcExtension;
         if (!move_uploaded_file($this->paFile["BPHOTO"]["tmp_name"], "Profiles/".$lcFoto)) {
            $this->pcError = "NO SE LOGRO SUBIR LA FOTO";
            return false;
         }
      }
      $this->paData['CFOTO'] = $lcFoto;
      $lcJson = json_encode($this->paData);
      $lcSql = "SELECT P_CREATEUSUARIO('$lcJson')";
      $R1 = $p_oSql->omExec($lcSql);
      $laFila = $p_oSql->fetch($R1);
      if (!isset($laFila[0])) {
         $this->pcError = "HA OCURRIDO UN PROBLEMA";
         return false;
      }
      $laJson = json_decode($laFila[0], true);
      if (isset($laJson['ERROR'])) {
         $this->pcError = $laJson['ERROR'];
         return false;
      }
      return true;
   }
}
?>