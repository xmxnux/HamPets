CREATE TABLE MUsuario(
    cidusua varchar(30),
    cnombre varchar(240),
    ccontra char(128),
    ccorreo char(50),
    ccelula char(11),
    cfoto varchar(50),
    dañnacim date,
    primary key(cidusua)
);

CREATE TABLE Moderador (
    nidalber integer REFERENCES MAlbergue(nidalbe)
)INHERITS(MUsuario); 

CREATE TABLE Empresa (
    cubicac char(60),
    cnombre varchar(50)
)INHERITS(MUsuario);

CREATE TABLE IEmpCateg(
    cidusua varchar(30) REFERENCES Empresa(cidusua),
    nidcate integer REFERENCES MCategoria(nidcate)
);

CREATE TABLE MCategoria(
    nidcate SERIAL PRIMARY KEY,
    cdescri varchar(20)
);

CREATE TABLE MRedSocial(
    nidrdso SERIAL PRIMARY KEY,
    cdescri varchar(20)
);

CREATE TABLE IEmpRedSoc(
    cidusua varchar(30) REFERENCES Empresa(cidusua),
    nidrdso integer REFERENCES MRedSocial(nidrdso) 
);

CREATE TABLE IAlberRedSo(
    nidalbe integer REFERENCES MAlbergue(nidalbe),
    nidrdso integer REFERENCES MRedSocial(nidrdso) 
);

CREATE TABLE MAlbergue(
    nidalbe SERIAL PRIMARY KEY, 
    cnombre varchar(60),
    cdirecc char(60),
    ccelula char(11),
    crefubi char(50)
);

CREATE TABLE IHorAlberg(
    nidalbe integer REFERENCES MAlbergue(nidalbe),
    nidhora integer REFERENCES MHorario(nidhora)
); 

CREATE TABLE MHorario(
    nidhora SERIAL PRIMARY KEY,
    horini time,
    horfin time
);

CREATE TABLE MPost(
    nidpost SERIAL PRIMARY KEY, 
    cidusua varchar(30) REFERENCES MUsuario(cidusua),
    dfecpub date,
    ctippos char(50),
    nestad integer REFERENCES DTipEst(nidest),
    cidmodr varchar(30) REFERENCES Moderador(cidusua),
    nidmasc integer REFERENCES MMascotas(nidmasc)
);

CREATE TABLE DTipEst(
    nidest SERIAL PRIMARY KEY,
    cdescri varchar(20)
);

CREATE TABLE MComentario(
    nidcome SERIAL PRIMARY KEY,
    nidpost integer REFERENCES MPost(sidpost),  
    cidusua varchar(30) REFERENCES MUsuario(cidusua),
    mdescri text
);

CREATE TABLE MMascotas(
    nidmasc SERIAL PRIMARY KEY,
    cnombre varchar(100),
    cgenero char(20),
    dEdad varchar(8),
    ctamaño char(30),
    ccolor char(30),
    craza char(30),
    mdetmed text,
    cfoto varchar(50)
);
CREATE TABLE MascUbAlber(
    nidalbe integer REFERENCES MAlbergue(nidalbe),  
    nañalbe integer 
)INHERITS(MMascotas);

CREATE TABLE IMascApadr(
);

CREATE TABLE MApadrinam(
    ctipApa char(15)
);

CREATE TABLE MascBuscEnc(
    cubicep char(60),
    dfechep date,
    cestado varchar(10)
)INHERITS(MMascotas);

CREATE TABLE MascAdopc(
)INHERITS(MMascotas);

CREATE TABLE TTablas (
    nSerial SERIAL UNIQUE,
    cCodTab CHAR(3) NOT NULL,
    nOrder  SMALLINT,
    cTblFld VARCHAR(100),
    cCodigo CHAR(8),
    cDescri VARCHAR(100),
    cTipReg CHAR(1)
);
INSERT INTO TTablas(DEFAULT, '001', 0, 'MMascotas.cRaza', '*', '*', 0)
INSERT INTO TTablas(DEFAULT, '001', 0, '*', '001', 'MESTIZO', 1)
INSERT INTO TTablas(DEFAULT, '002', 0, 'MMascotas.cSexo', '*', '*', 0)
INSERT INTO TTablas(DEFAULT, '002', 0, '*', 'M', 'MACHO',  1)
INSERT INTO TTablas(DEFAULT, '002', 0, '*', 'H', 'HEMBRA', 1)