CREATE EXTENSION pgcrypto;
CREATE TABLE MUsuario(
    cIdUsua varchar(30) primary key,
    cNombre varchar(240),
    cContra char(128),
    cCorreo char(50),
    cCelula char(11),
    cFoto   varchar(50),
    dAñNaci CHAR(4)
);
CREATE OR REPLACE FUNCTION P_CREATEUSUARIO(p_cData TEXT) RETURNS TEXT AS
$$
DECLARE
    lcData JSON;
    lcNombre VARCHAR(240) NOT NULL := '';
    lcContra TEXT NOT NULL := '';
    lcCorreo CHAR(50) NOT NULL := '';
    lcCelula CHAR(11) NOT NULL := '';
    lcFoto   VARCHAR(50) := '';
    ldAñNaci CHAR(4) NOT NULL := '';
BEGIN
    BEGIN
        lcData := p_cData::JSON;
        lcNombre := lcData->>'CNOMBRE';
        lcContra := lcData->>'CCONTRA';
        lcCorreo := lcData->>'CCORREO';
        lcCelula := lcData->>'CCELULA';
        lcFoto   := lcData->>'CFOTO';
        ldAñNaci := lcData->>'CAÑNACI';
    EXCEPTION WHEN OTHERS THEN
        RETURN '{"ERROR": "PARAMETRO NO ES JSON"}';
    END;
    IF (SELECT COUNT(1) FROM MUsuario WHERE cCorreo = lcCorreo) > 0 THEN
        RETURN '{"ERROR":"CORREO ELECTRONICO YA REGISTRADO"}';
    END IF;
    IF (SELECT COUNT(1) FROM MUsuario WHERE cCelula = lcCelula) > 0 THEN
        RETURN '{"ERROR":"CELULAR YA REGISTRADO"}';
    END IF;
    INSERT INTO MUsuario VALUES(SUBSTRING(lcCorreo, '(.*)@'), lcNombre, ENCODE(DIGEST(lcContra, 'sha512'), 'hex'), lcCorreo, lcCelula, lcFoto, ldAñNaci);
    RETURN '{"OK":"OK"}';
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION P_LOGIN(p_cData TEXT) RETURNS TEXT AS 
$$
DECLARE
    lcData   JSON;
    lcIdUsua VARCHAR(30);
    lcContra TEXT;
    R1       RECORD;
BEGIN
    BEGIN
        lcData := p_cData::JSON;
        lcIdUsua := lcData->>'CIDUSUA';
        lcContra := lcData->>'CCONTRA';
    EXCEPTION WHEN OTHERS THEN
        RETURN '{"ERROR": "PARAMETRO NO ES JSON"}';
    END;
    SELECT ENCODE(DIGEST(lcContra, 'sha512'), 'hex') as cNHash, cIdUsua, cNombre, cContra, cCorreo, cCelula, cFoto, dAñNaci     
    INTO R1 FROM MUsuario WHERE cIdUsua = lcIdUsua;
    IF R1 IS NULL THEN
        RETURN '{"ERROR":"NO EXISTE USUARIO"}';
    END IF;
    IF R1.cNHash = R1.cContra THEN
        RETURN '{"CIDUSUA":"' || R1.cIdUsua || '", "CNOMBRE":"' || R1.cNombre || '", "CCORREO":"' || R1.cCorreo || '","CCELULA":"' || R1.cCelula || '","CFOTO":"' || R1.cFoto || '","DAÑNACI":"' || R1.dAñNaci || '"}';
    END IF;
    RETURN '{"ERROR":"CONTRASEÑA INCORRECTA"}';
END
$$ LANGUAGE plpgsql;