<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CBase.php';
   require_once 'Clases/CLogin.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxValidateSesion()) {
      return;
   } else {
      fxInit();
   }
   function fxInit() {
      fxScreen(0);
   }
   function fxScreen($p_nFlag) {
		global $loSmarty;
		//$loSmarty->assign('saDatos',  $_SESSION['paDatos']);
		//$loSmarty->assign('saData',   $_SESSION['paData']); 
		$loSmarty->assign('snBehavior', $p_nFlag);
		$loSmarty->display('Plantillas/Adopta.tpl');
   }
?>