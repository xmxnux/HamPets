<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CBase.php';
   require_once 'Clases/CLogin.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxValidateSesion()) {
      return;
   } elseif (@$_REQUEST['Boton'] == 'Iniciar') {
   	fxIniciarSesion();
   } elseif (@$_REQUEST['Boton'] == 'CerrarSesion') { 
      fxCerrarSesion();
   } else {
      fxInit();
   }
   function fxInit() {
		fxScreen(0);
   }
   function fxCerrarSesion() {
      session_destroy();
      fxHeader('index.php');
   }
   function fxScreen($p_nFlag) {
		global $loSmarty;
		//$loSmarty->assign('saDatos',  $_SESSION['paDatos']);
		//$loSmarty->assign('saData',   $_SESSION['paData']); 
		$loSmarty->assign('snBehavior', $p_nFlag);
		$loSmarty->display('Plantillas/Menu.tpl');
   }
?>