<html>
<head>
   <title>HamPets!</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.min.js"></script>
   <!--APP MANIFEST AND LOGOS-->
   {CBase::getHTMLConfig()}
   <meta name="theme-color" content="#7e4871">
   <script>
      $(document).ready(function() {
         $('#register_form').submit(function(){
            let password = $('input[name="paData[CCONTRA]"]').val();
            let confirm_password = $('input[name="paData[CREPCON]"]').val();
            if (password != confirm_password) {
               $('input[name="paData[CREPCON]"]').val('');
               return false;
            }
            return true;
         });
         if (location.hash == "#login") {
            history.pushState(null, "HamPets!", "#main");
            omLogin();
         }
         else if (location.hash == "#register") {
            history.pushState(null, "HamPets!", "#main");
            omRegistrarse();
         }
      });
      window.onhashchange = function(a) {
         $('#logoHeading').css('margin-top', '100px');
         $(".background_util").fadeOut('normal');
         $('#registrar_panel').fadeOut('fast');
         $('#login_panel').fadeOut('fast', function() {
            $('#main_panel').fadeIn('slow');
         });
         $("body").attr('style', "background-image: url('Images/fondo_principal.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center");
      }
      function omLogin() {
         history.pushState(null, 'Iniciar Sesión', '#login');
         $('#logoHeading').css('margin-top', '2px');
         //$(".login_background").fadeIn('slow');
         $('#main_panel').fadeOut('fast', function() {
            $('#login_panel').fadeIn('slow');
         }); 
         $("body").attr('style', "background-image: url('Images/fondo_principal_login.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center");
      }
      function omRegistrarse() {
         history.pushState(null, 'Registro', '#register');
         $('#logoHeading').css('margin-top', '2px');
         $(".background_util").fadeIn('slow');
         $('#main_panel').fadeOut('fast', function() {
            $('#registrar_panel').fadeIn('slow');
         });         
         $("body").attr('style', "background: none");
      }
      function fileLoaded() {
         let lbPhoto = $('input[name="BPHOTO"]')[0];
         if (lbPhoto.files && lbPhoto.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
               $('#profile-photo').css('background-image', 'url(' + e.target.result + ')');
               $('#profile-photo').css('background-size', 'cover');
            }
            reader.readAsDataURL(lbPhoto.files[0]);
         }
      }
      function loadImage() {
         $('input[name="BPHOTO"]').trigger('click');
      }
      function toggleFooter() {
         $('#txofDialog').toggleClass('hidden');
      }
   </script>
</head>
<body style="background-image: url('Images/fondo_principal.jpg'); background-size: cover; background-position: top">
   <div class="background_util" style="display: none; position: fixed; top: 0; right: 0; height: 100%; width: 100%; overflow: hidden; background-image: url('Images/fondo_principal_login.jpg'); background-repeat: no-repeat; background-size: 100%; background-position: right top"></div>
   <div class="background_util" style="display: none; position: fixed; top: 0; right: 0; height: 25%;  width: 100%; overflow: hidden; background-image: url('Images/fondo_principal_login.jpg'); background-repeat: no-repeat; background-size: 100%; background-position: right top; z-index: 4;"></div>
   <div class="row"></div>
   <div id="logoHeading" class="panel-heading" style="transition: .5s; margin-top: 100px; text-align: center; z-index: 4; position: fixed; width: 100%">
      <img src="Images/HamPetsLogo.png" style="height: 9%;">
      <h4 style="color: black; font-weight: bold; font-size: 2rem; text-shadow: 2px 4px 8px rgba(0, 0, 0, .5)">Comunidad de purritos y gaticos</h4>
   </div>   
   <div class="container-fluid">      
      <form action="index.php" id="register_form" method="POST" enctype="multipart/form-data">
         <div id="registrar_panel" style="display: none">
            <div class="col-xs-10 col-xs-push-1" style="margin-top: 100px">
               <div class="row">
                  <h3>Registrarse</h3>
               </div>
               <div id="profile-photo" class="upload-background profile-photo center-block" onclick="loadImage()"></div>
               <input onchange="fileLoaded()" type="file" accept="image/*" name="BPHOTO" style="display: none">
               <br><b>Nombre</b>
               <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input type="text" class="form-control btn-lg" name="paData[CNOMBRE]" placeholder="Nombre" autofocus/> 
               </div>
               <br><b>Celular</b>
               <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input type="text" maxlength="9" minlength="9" class="form-control btn-lg" name="paData[CCELULA]" placeholder="Celular" autofocus/> 
               </div>
               <br><b>Año de nacimiento</b>
               <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input type="text" minlength="4" maxlength="4" class="form-control btn-lg" name="paData[CAÑNACI]" placeholder="Año de Nacimiento" autofocus/> 
               </div>
               <br><b>Correo Electrónico</b>
               <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input type="email" class="form-control btn-lg" name="paData[CCORREO]" placeholder="Correo Electrónico" autofocus/> 
               </div>
               <br><b>Contraseña</b>
               <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input type="password" class="form-control btn-lg" name="paData[CCONTRA]" placeholder="Contraseña"/> 
               </div>
               <br><b>Repetir Contraseña</b>
               <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input type="password" class="form-control btn-lg" name="paData[CREPCON]" placeholder="Repetir Contraseña"/> 
               </div>
               {if isset($saError) && $saError['pcAction'] == 'Registrar'}
               <div class="alert alert-danger alert-dismissible" style="margin-top: 10px">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Error: </strong> {$saError['pcMsg']}
               </div>
               {/if}
               <div class="row">
                  <button type="submit" name="Boton" value="Registrar" class="block-center gradiente col-xs-12 col-md-4 col-md-push-4" style="display:inline-block">Registrarse</button>
               </div>
               <div class="row"></div>
               <div class="row"></div>
            </div>
         </div>
      </form>
      <form action="index.php" method="POST">
         <div id="login_panel" style="display: none;">
            <div class="col-xs-10 col-xs-push-1" style="margin-top: 100px">
               <div class="row">
                  <h3>Iniciar Sesión</h3>
               </div>
               <br><b>Usuario</b>
               <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input type="text" class="form-control btn-lg" name="paData[CIDUSUA]" placeholder="Usuario" autofocus/> 
               </div>
               <br><b>Contraseña</b>
               <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <input type="password" class="form-control btn-lg" name="paData[CCONTRA]" placeholder="Contraseña"/> 
               </div>
               {if isset($saError) && $saError['pcAction'] == 'IniciarSesion'}
               <div class="alert alert-danger alert-dismissible" style="margin-top: 10px">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Error: </strong> {$saError['pcMsg']}
               </div>
               {/if}
               <div class="row">
                  <button type="submit" name="Boton" value="IniciarSesion" class="block-center gradiente col-xs-12 col-md-4 col-md-push-4" style="display:inline-block">Iniciar Sesión</button>
               </div>
               <div class="row"></div>
            </div>
         </div>
         <div id="main_panel" class="row" style="margin-top: 210px">
            <div class="col-xs-8 col-xs-push-2 col-md-6 col-md-push-3">
               <div class="row">
                  <button type="button" onclick="omLogin()" class="block-center gradiente col-xs-12 col-md-6 col-md-push-3" style="display:inline-block">Iniciar sesión</button>
               </div>
               <div class="row">
                  <button type="button" onclick="omRegistrarse()" class="block-center gradiente col-xs-12 col-md-6 col-md-push-3" style="display:inline-block">Registrarse</button>
               </div>
            </div>
         </div>
      </form>
   </div>   
   <div class="login_background" style="display: none; position: fixed; top:0; right: 0; height: 25%; width: 100%; overflow: hidden; background: inherit; z-index: 3;">
      <div class="wave" style="background-image: url('Images/waves.png'); background-size: 50% 75px; animation: waves 4s linear infinite; opacity: .7; z-index: -1"></div>
      <div class="wave" style="background-image: url('Images/waves.png'); background-size: 50% 75px; animation: waves 7s linear infinite; opacity: .9; z-index: -2"></div>
      <div class="wave" style="background-image: url('Images/waves.png'); background-size: 50% 75px; animation: waves 10s linear infinite; opacity: 1; z-index: -3"></div>
   </div>
   <div class="login_background" style="display: none; background-color: white; position: fixed; top: 25%; height: 75%; width: 100%; z-index: -1;"></div>
   <div class="footer" onclick="toggleFooter()">
      <img src="Images/TXOFLogoFooter.png" style="height: 75%; position: absolute; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%)">
   </div>
   <div id="txofDialog" class="hidden">
      <div class="dialogBackground" onclick="toggleFooter()"></div>
      <div style="background-color: #222; width: 90%; height: 50%; position: absolute; top: 25%; left: 5%; border-radius: 10px;">
         <span style="position: absolute; right: 12px; top: 0; font-size: xx-large; color: white" onclick="toggleFooter()">&times;</span>
         <img src="Images/TXOFLogo.png" style="width: 70%; margin-left: 15%; margin-top: 30px; margin-bottom: 15px">
         <p style="margin-left: 5%; width: 90%; background-color: lightgray; border-radius: 3px; color: black; padding: 2px 4px;">Somos una empresa dedicada al desarollo de software</p>
         <a href="#" style="width: 100%; position: absolute; bottom: 0; border-radius: 10px; text-align: center; background-color: white; padding: 4px 0">Ir al portal</a>
      </div>
   </div>
   <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">
         <form action="index.php" method="POST">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title">Restablecer Contraseña</h4>
            </div>
            <div class="modal-body">
               <p>Correo electronico:<input name="paData[CEMAIL]" class="form-control" type="email" required></p>
            </div>
            <div class="modal-footer">
               <button class="btn btn-success" name="Boton" value="Restablecer">Restablecer</button>
               <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
         </div>
         </form>
      </div>
   </div>   
</body>
</html>