<html>
<head>
   <title>HamPets!</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.min.js"></script>
   <!--APP MANIFEST AND LOGOS-->
   {CBase::getHTMLConfig()}
   <script>
      /* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
      function openNav() {
         if ($(window).width() > 768) {
            document.getElementById("mySidenav").style.width = "30%";
         }
         else {
            document.getElementById("mySidenav").style.width = "65%";
         }
         document.getElementById("mySideBackground").style.width = "100%";
      }

      /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
      function closeNav() {
         document.getElementById("mySidenav").style.width = "0";
         document.getElementById("mySideBackground").style.width = "0";
      }

      function toggleFooter() {
         $('#txofDialog').toggleClass('hidden');
      }

      function selectMenuOption(option) {
         setTimeout(launchOption(option), 3000);
      }

      function launchOption(option) {
         window.location.href = option + ".php";
      }
      function selectSize(self) {
         let loCurInp = $('input[name="paData[SIZE]"]');
         let loCurImg = $('img[val="' + loCurInp.val() + '"]');
         console.log(loCurImg);
         // CAMBIA IMAGEN SELECCIONADA POR NORMAL
         let lcNorImg = "Images/" + loCurImg.attr('image') + ".png";
         loCurImg.attr('src', lcNorImg)
         //loCurrent
         // NUEVA SELECCION
         let lcSelImg = "Images/" + self.getAttribute('image') + "S.png";
         self.setAttribute('src', lcSelImg);
         // ESTABLECER INPUT
         loCurInp.attr('value', self.getAttribute('val'));
      }
   </script>
</head>
<body style="background-image: url('Images/background2.jpg')">
<div class="top_menu">
   <div>
      <img class="center-block" src="Images/HamPetsLogo.png" style="height:80%; position: absolute; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%)">
   </div>
   <span onclick="openNav()">&#9776;</span>
</div>
<div id="main" class="container-fluid">
   <div class="col-xs-12 col-lg-8 col-lg-push-2" style="margin-top: 15%;">
      <div class="panel panel-danger">
         <div class="panel-heading">Datos de la mascota</div>
         <div class="panel-body">
            <table class="table table-condensed">
               <tr>
                  <th>Tipo</th>
               </tr>
               <tr>
                  <td style="font-size: 4rem; text-align: center">
                     🐶
                     🐱
                  </td>
               </tr>
               <tr>
                  <th>Raza</th>
               </tr>
               <tr>
                  <td>
                     <select class="form-control select-picker">
                        <option>MESTIZO</option>
                     </select>
                  </td>
               </tr>
               <tr>
                  <th>Género</th>
               </tr>
               <tr>
                  <td>
                     <!--<img src="Images/MGender.png" style="width: 20%">
                     <img src="Images/FGender.png" style="width: 20%">-->
                     <select class="form-control select-picker">
                        <option value="" selected disabled>Selecione una opción</option>
                        <option>MACHO</option>
                        <option>HEMBRA</option>
                     </select>
                  </td>
               </tr>
               <tr>
                  <th>Edad</th>
               </tr>
               <tr>
                  <td>
                     <div class="col-xs-6">
                        <input placeholder="EDAD" type="number" min="1" max="12" class="form-control">
                     </div>
                     <div class="col-xs-6">
                        <select class="form-control">
                           <option>MESES</option>
                           <option>AÑOS</option>
                        </select>
                     </div>
                  </td>
               </tr>
               <tr>
                  <th>Tamaño</th>
               </tr>
               <tr>
                  <td style="text-align: center">
                     <input type="hidden" name="paData[SIZE]" value="small">
                     <img onclick="selectSize(this)" image="PetSizeSmall" val="small" src="Images/PetSizeSmallS.png" style="width: 25%">
                     <img onclick="selectSize(this)" image="PetSizeMedium" val="medium" src="Images/PetSizeMedium.png" style="width: 25%">
                     <img onclick="selectSize(this)" image="PetSizeLarge" val="large" src="Images/PetSizeLarge.png" style="width: 25%">
                  </td>
               </tr>
               <tr>
                  <th>Color</th>
               </tr>
               <tr>
                  <td style="text-align: center">
                     <div style="display: inline-block; background-color: white; width: 18px; height: 18px; border-radius: 9px; border: 1px solid black"></div>
                     <div style="display: inline-block; background-color: black; width: 18px; height: 18px; border-radius: 9px;"></div>
                     <div style="display: inline-block; background-color: #8a5b5b; width: 18px; height: 18px; border-radius: 9px;"></div>
                     <div style="display: inline-block; background-color: gray;  width: 18px; height: 18px; border-radius: 9px;"></div>
                     <div style="display: inline-block; background-color: peru;  width: 18px; height: 18px; border-radius: 9px;"></div>
                  </td>
               </tr>
            </table>
         </div>
         <div class="panel-footer">
            <button class="btn btn-danger btn-block">Registrar</button>
         </div>
      </div>
   </div>
</div>
<div id="mySideBackground" onclick="closeNav()" style="height: 100%; width: 0; position: fixed; top: 0; left: 0; background-color: rgba(0, 0, 0, 0.5)"></div>
<div id="mySidenav" class="sidenav">
   <form action="menu.php" method="POST">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <a href="#"><img src="Images/heart.png" style="width: 35px">  Adopta</button>
      <a href="#"><img src="Images/match.png" style="width: 35px">  Match</a>
      <a href="#"><img src="Images/find.png" style="width: 35px">  ¿Encontraste una mascota extraviada?</a>
      <a href="#"><img src="Images/missing.png" style="width: 35px">  Lista de Mascótas Perdidas</a>
      <a href="#"><img src="Images/others.png" style="width: 35px">  Otros</a>
      <button name="Boton" value="CerrarSesion"><img src="Images/kitty.png" style="width: 35px">  Cerrar Sesión</button>
   </form>
</div>
<div class="footer" onclick="toggleFooter()">
   <img src="Images/TXOFLogoFooter.png" style="height: 75%; position: absolute; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%)">
</div>
<div id="txofDialog" class="hidden">
   <div class="dialogBackground" onclick="toggleFooter()"></div>
   <div style="background-color: #222; width: 90%; height: 50%; position: absolute; top: 25%; left: 5%; border-radius: 10px;">
      <span style="position: absolute; right: 12px; top: 0; font-size: xx-large; color: white" onclick="toggleFooter()">&times;</span>
      <img src="Images/TXOFLogo.png" style="width: 70%; margin-left: 15%; margin-top: 30px; margin-bottom: 15px">
      <p style="margin-left: 5%; width: 90%; background-color: lightgray; border-radius: 3px; color: black; padding: 2px 4px;">Somos una empresa dedicada al desarollo de software</p>
      <a href="#" style="width: 100%; position: absolute; bottom: 0; border-radius: 10px; text-align: center; background-color: white; padding: 4px 0">Ir al portal</a>
   </div>
</div>
</body>
</html>