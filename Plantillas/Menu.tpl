<html>
<head>
   <title>HamPets!</title>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
   <link rel="stylesheet" href="Styles/css/bootstrap.css">
   <link rel="stylesheet" href="CSS/style.css">
   <script src="Styles/js/jquery-3.2.0.js"></script>
   <script src="Styles/js/bootstrap.min.js"></script>
   <!--APP MANIFEST AND LOGOS-->
   {CBase::getHTMLConfig()}
   <script>
      /* Set the width of the side navigation to 250px and the left margin of the page content to 250px and add a black background color to body */
      function openNav() {
         if ($(window).width() > 768) {
            document.getElementById("mySidenav").style.width = "30%";
         }
         else {
            document.getElementById("mySidenav").style.width = "65%";
         }
         document.getElementById("mySideBackground").style.width = "100%";
      }

      /* Set the width of the side navigation to 0 and the left margin of the page content to 0, and the background color of body to white */
      function closeNav() {
         document.getElementById("mySidenav").style.width = "0";
         document.getElementById("mySideBackground").style.width = "0";
      }

      function toggleFooter() {
         $('#txofDialog').toggleClass('hidden');
      }

      function selectMenuOption(option) {
         setTimeout(launchOption(option), 3000);
      }

      function launchOption(option) {
         window.location.href = option + ".php";
      }
   </script>
</head>
<body style="background-image: url('Images/background2.jpg')">
<div class="top_menu">
   <div>
      <img class="center-block" src="Images/HamPetsLogo.png" style="height:80%; position: absolute; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%)">
   </div>
   <span onclick="openNav()">&#9776;</span>
</div>
<div id="main" class="container-fluid">
   <div class="row">
      <div class="col-xs-10 col-lg-8 col-lg-push-2 col-xs-push-1" style="margin-top: 15%;">
         <div onclick="selectMenuOption('adopta')" class="panel panel-success col-xs-5 square-4 col-sm-3 gradent gradent-1">
            <div style="position: absolute; width: 100%; height: 100%; top: 0; left: 0"></div>
            <span class="text-responsive">Dar en adopción</span>
            <img class="sqr_hidden" src="Images/square.png">            
         </div>
         <div class="col-xs-2 square-4 col-sm-1">
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent gradent-2">
            <div style="position: absolute; width: 100%; height: 100%; top: 0; left: 0"></div>
            <span class="text-responsive">Match</span>
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <div class="col-xs-2 square-4 col-sm-1 hidden-xs">
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent gradent-3">
            <div style="position: absolute; width: 100%; height: 100%; top: 0; left: 0"></div>
            <span class="text-responsive">¿Encontraste una mascota extraviada?</span>
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <div class="col-xs-2 square-4 col-sm-1">
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent gradent-4">
            <div style="position: absolute; width: 100%; height: 100%; top: 0; left: 0"></div>
            <span class="text-responsive">Lista de Mascotas Perdidas</span>
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <div class="col-xs-2 square-4 col-sm-1 hidden-xs">
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent gradent-5">
            <div style="position: absolute; width: 100%; height: 100%; top: 0; left: 0"></div>
            <span class="text-responsive">Denuncia</span>
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <div class="col-xs-2 square-4 col-sm-1">
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent gradent-1">
            <div style="position: absolute; width: 100%; height: 100%; top: 0; left: 0"></div>
            <span class="text-responsive">¿Cómo ser voluntario?</span>
            <img class="sqr_hidden" src="Images/square.png">
         </div>
         <!--<div class="panel" style="background: transparent">
            <div><h4><b>Otros</b></h4></div>
            <div class="col-xs-2 square-4 col-sm-1 hidden-xs">
               <img class="sqr_hidden" src="Images/square.png">
            </div>
            <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent">
               <span style="color: black">Da en adopción</span>
               <img class="sqr_hidden" src="Images/square.png">
            </div>
            <div class="col-xs-2 square-4 col-sm-1">
               <img class="sqr_hidden" src="Images/square.png">
            </div>
            <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent">
               <span style="color: black">Apadrina</span>
               <img class="sqr_hidden" src="Images/square.png">
            </div>
            <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent">
               <span style="color: black">Búscas comprar algo?</span>
               <img class="sqr_hidden" src="Images/square.png">
            </div>
            <div class="col-xs-2 square-4 col-sm-1">
               <img class="sqr_hidden" src="Images/square.png">
            </div>
            <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent">
               <span style="color: black">Cómo ser voluntario?</span>
               <img class="sqr_hidden" src="Images/square.png">
            </div>
            <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent">
               <span style="color: black">Denuncia</span>
               <img class="sqr_hidden" src="Images/square.png">
            </div>
            <div class="col-xs-2 square-4 col-sm-1">
               <img class="sqr_hidden" src="Images/square.png">
            </div>
            <div class="panel panel-success col-xs-5 square-4 col-sm-3 gradent">
               <span style="color: black">Campañas</span>
               <img class="sqr_hidden" src="Images/square.png">
            </div>
         </div>-->
      </div>
   </div>
   <div class="row">
      
   </div>
</div>
<div id="mySideBackground" onclick="closeNav()" style="height: 100%; width: 0; position: fixed; top: 0; left: 0; background-color: rgba(0, 0, 0, 0.5)"></div>
<div id="mySidenav" class="sidenav">
   <form action="menu.php" method="POST">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <a href="#"><img src="Images/heart.png" style="width: 35px">  Adopta</button>
      <a href="#"><img src="Images/match.png" style="width: 35px">  Match</a>
      <a href="#"><img src="Images/find.png" style="width: 35px">  ¿Encontraste una mascota extraviada?</a>
      <a href="#"><img src="Images/missing.png" style="width: 35px">  Lista de Mascótas Perdidas</a>
      <a href="#"><img src="Images/others.png" style="width: 35px">  Otros</a>
      <button name="Boton" value="CerrarSesion"><img src="Images/kitty.png" style="width: 35px">  Cerrar Sesión</button>
   </form>
</div>
<div class="footer" onclick="toggleFooter()">
   <img src="Images/TXOFLogoFooter.png" style="height: 75%; position: absolute; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%)">
</div>
<div id="txofDialog" class="hidden">
   <div class="dialogBackground" onclick="toggleFooter()"></div>
   <div style="background-color: #222; width: 90%; height: 50%; position: absolute; top: 25%; left: 5%; border-radius: 10px;">
      <span style="position: absolute; right: 12px; top: 0; font-size: xx-large; color: white" onclick="toggleFooter()">&times;</span>
      <img src="Images/TXOFLogo.png" style="width: 70%; margin-left: 15%; margin-top: 30px; margin-bottom: 15px">
      <p style="margin-left: 5%; width: 90%; background-color: lightgray; border-radius: 3px; color: black; padding: 2px 4px;">Somos una empresa dedicada al desarollo de software</p>
      <a href="#" style="width: 100%; position: absolute; bottom: 0; border-radius: 10px; text-align: center; background-color: white; padding: 4px 0">Ir al portal</a>
   </div>
</div>
</body>
</html>