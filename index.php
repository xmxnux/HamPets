<?php
   require_once 'Libs/Smarty.class.php';
   require_once 'Clases/CBase.php';
   require_once 'Clases/CSql.php';
   require_once 'Clases/CLogin.php';
   require_once 'Clases/CUsuario.php';
   session_start();
   date_default_timezone_set('America/Bogota');
   $loSmarty = new Smarty;
   if (!fxHasSesion()) {
      return;
   } elseif (@$_REQUEST['Boton'] == 'Iniciar') {
   	fxIniciarSesion();
   } elseif (@$_REQUEST['Boton'] == 'IniciarSesion') {
   	fxIniciarSesion();
   } elseif(@$_REQUEST['Boton'] == 'Registrar') {
      fxRegistrar();
   } else {
      fxInit();
   }
   function fxInit() {
		fxScreen(0);
   }
   function fxIniciarSesion() {
      $lo = new CLogin();
      $lo->paData = $_REQUEST['paData'];
      $llOk = $lo->omLogin();
      if ($llOk) {
         $_SESSION['GADATA'] = $lo->paDatos;
         fxHeader2('menu.php');
      }
      else {
         fxError($lo->pcError, $_REQUEST['Boton']);
         fxHeader2('index.php#login');
      }
   }
   function fxRegistrar() {
      $lo = new CUsuario();
      $lo->paData = $_REQUEST['paData'];
      $lo->paFile = $_FILES;
      $llOk = $lo->omRegistrarUsuario();
      if (!$llOk) {
         fxError($lo->pcError, $_REQUEST['Boton']);
         fxHeader2('index.php#login');
         //fxHeader('index.php', $lo->pcError);
         return;
      }
      fxAlert('REGISTRADO CORRECTAMENTE');
      fxInit();
   }
   function fxScreen($p_nFlag) {
		global $loSmarty;
		//$loSmarty->assign('saDatos',  $_SESSION['paDatos']);
      //$loSmarty->assign('saData',   $_SESSION['paData']); 
      if (isset($_SESSION['paError'])) {
         $loSmarty->assign('saError', $_SESSION['paError']);
         unset($_SESSION['paError']);
      }      
		$loSmarty->assign('snBehavior', $p_nFlag);
		$loSmarty->display('Plantillas/Index.tpl');
   }
?>